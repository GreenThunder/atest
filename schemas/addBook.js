module.exports = {
    type: 'object',
    required: ['title', 'desc', 'image', 'authors_id'],
    properties: {
        title: { type: 'string' },
        desc: { type: 'string' },
        image: { type: 'string' },
        authors_id: { type: 'string' },
    },
    additionalProperties: false,
};
