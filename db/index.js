const { cacheQuery, connection } = require('./mysql')

module.exports = {
    cacheQuery,
    connection
}

