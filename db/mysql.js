const mysql = require("mysql2");
const crypto = require('crypto');
const redis = require('./redis');

const config = require('../lib/config')

const CACHE_TTL = 1800
  
const connection = mysql.createConnection(config.db).promise();
 connection.connect(function(err){
    if (err) {
      return console.error("Error: " + err.message);
    }
    else{
      console.log("Connected to mysql");
    }
 });

 const cacheQuery = (sql, filter) => {
     return new Promise(async (resolve, reject) => {
         let hash = crypto.createHash('md5').update(filter ? sql + JSON.stringify(filter) : sql).digest('hex');

         const cached = await redis.get(hash)

         if (cached) {
             resolve(JSON.parse(cached))
         } else {
             const author = await connection.query(sql, filter)
             if (author[0][0]) {
                 resolve(author[0])

                 redis.set(hash, JSON.stringify(author[0]), 'ex', CACHE_TTL)
             } else {
                 reject(404)
             }
         }
     })
}
module.exports = {
    cacheQuery,
    connection
};
