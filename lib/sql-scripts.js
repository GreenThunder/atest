const createAuthorsTable = `create table if not exists 'authors'(
  'id' int not null auto_increment,
  'name' varchar(255) not null,
  primary key('id'),
  unique key('name') 
);`.replace(/'/g, '`');

const createBooksTable = `create table if not exists 'books'(
  'id' int not null auto_increment,
  'title' varchar(255) not null,
  'date' varchar(255) not null,
  'description' varchar(255) not null,
  'image' varchar(255) not null,
  primary key('id')
);`.replace(/'/g, '`');


const createBookAuthorsTable = `create table if not exists 'book_authors'(
  'book_id' int not null,
  'author_id' int not null,
  foreign key ('book_id') references 'books' ('id') ON DELETE CASCADE ON UPDATE CASCADE,
  foreign key ('author_id') references 'authors' ('id') ON DELETE CASCADE ON UPDATE CASCADE,
  primary key ('book_id', 'author_id')
);`.replace(/'/g, '`');


const dropAuthorsTable = 'drop table if exists `authors`';

const dropBooksTable = 'drop table if exists `books`';

const dropBookAuthorsTable = 'drop table if exists `book_authors`';

module.exports = {
    createAuthorsTable,
    createBooksTable,
    createBookAuthorsTable,
    dropAuthorsTable,
    dropBooksTable,
    dropBookAuthorsTable,
}