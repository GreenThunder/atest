const Router = require('koa-router')
const bodyParser = require('koa-bodyparser');
const Ajv = require('ajv');

const ajv = new Ajv({ allErrors: true });

const { cacheQuery, connection} = require('../db');
const { buildFilterValue, buildFilterOption } = require('../helpers/queryOptions');
const addBookSchema = require('../schemas/addBook');

const router = new Router().prefix('/books')

router.post('/', bodyParser(), async (ctx) => {
    if (!ajv.validate(addBookSchema, ctx.request.body)) {
        ctx.status = 400
    }

    const { title, desc, image, authors_id } = ctx.request.body

    let authors_id_arr = []
    if (authors_id.includes(',')) {
        authors_id_arr = authors_id.split(',')
    } else {
        authors_id_arr.push(authors_id)
    }

    try {
        const sql = `INSERT INTO books(title, description, image, date) VALUES(?, ?, ?, ?)`
        let result = await connection.query(sql, [title, desc, image, Date.now()])

        const book_authors_sql = `INSERT INTO book_authors(author_id, book_id) VALUES(?, ?)`
        authors_id_arr.forEach(async author => {
            try {
                await connection.query(book_authors_sql, [author, result[0].insertId])
            } catch (err) {
                //author missing
            }
        })

        ctx.body = result
        ctx.status = 201
    } catch (err) {
        ctx.body = err
        ctx.status = 500
    }
})

router.get('/', async (ctx) => {
    const { query } = ctx
    const { limit, offset, title, id, desc, image, date, sortByTitle, sortByDesc, sortByImage, sortByDate, authorsConcat } = query

    let filterValues = ""
    let filterBy = []

    filterValues += buildFilterValue('filter', 'title', title, filterValues, 'books', ' AND', ' WHERE')
    filterBy = buildFilterOption(title, filterBy)


    filterValues += buildFilterValue('filter', 'id', id, filterValues, 'books', ' AND', ' WHERE')
    filterBy = buildFilterOption(id, filterBy)

    filterValues += buildFilterValue('filter', 'description', desc, filterValues, 'books', ' AND', ' WHERE')
    filterBy = buildFilterOption(desc, filterBy)

    filterValues += buildFilterValue('filter', 'image', image, filterValues, 'books', ' AND', ' WHERE')
    filterBy = buildFilterOption(image, filterBy)

    filterValues += buildFilterValue('filter', 'date', date, filterValues, 'books', ' AND', ' WHERE')
    filterBy = buildFilterOption(date, filterBy)

    let sortOptions = ""
    sortOptions += buildFilterValue('order', 'title', sortByTitle, sortOptions, 'books', ',', ' ORDER BY')
    sortOptions += buildFilterValue('order', 'description', sortByDesc, sortOptions, 'books', ',', ' ORDER BY')
    sortOptions += buildFilterValue('order', 'image', sortByImage, sortOptions, 'books', ',', ' ORDER BY')
    sortOptions += buildFilterValue('order', 'date', sortByDate, sortOptions, 'books', ',', ' ORDER BY')

    const sql = `SELECT books.id, ${authorsConcat ? 'GROUP_CONCAT(authors.name)' : 'authors.name'}, books.title, books.description, books.image, books.date
          FROM authors
            INNER JOIN book_authors
          ON book_authors.author_id = authors.id
            INNER JOIN books
          ON books.id = book_authors.book_id${filterValues}${sortOptions}${limit ? ` LIMIT ${limit}` : ''}${offset ? ` OFFSET ${offset}` : ''}`


    try {
        ctx.body = await cacheQuery(sql, filterBy)
    } catch (err) {
        ctx.throw(err)
    }
})

router.delete('/:id', async (ctx) => {
    try {
        const sql = `DELETE FROM books WHERE id= ?`
        ctx.body = await connection.query(sql, ctx.params.id)
        ctx.status = 202
    } catch (err) {
        ctx.body = err
        ctx.status = 500
    }
})

router.put('/', bodyParser(), async (ctx) => {
    const { id, title, desc, image, authors_id } = ctx.request.body

    if (authors_id) {
        let authors_id_arr = []
        if (authors_id.includes(',')) {
            authors_id_arr = authors_id.split(',')
        } else {
            authors_id_arr.push(authors_id)
        }

        let sql = `INSERT INTO book_authors(author_id, book_id) VALUES(?, ?)`
        authors_id_arr.forEach(async author => {
            try {
                await connection.query(sql, [author, id])
            } catch (err) {
                //author missing
            }
        })
    }

    try {
        let sql = `UPDATE books SET title = ?, description = ?, image = ? WHERE id = ?`
        ctx.body = await connection.query(sql, [title, desc, image, id])
        ctx.status = 202
    } catch (err) {
        ctx.body = err
        ctx.status = 500
    }
})

module.exports = router.routes()
