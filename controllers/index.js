const Router = require('koa-router')

const authors = require('./authors')
const books = require('./books')

const router = new Router().prefix('/api')

router.use(authors, books)

module.exports = router
