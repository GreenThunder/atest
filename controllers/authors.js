const Router = require('koa-router')
const bodyParser = require('koa-bodyparser');

const { cacheQuery, connection} = require('../db');
const { buildFilterValue, buildFilterOption } = require('../helpers/queryOptions');

const router = new Router().prefix('/authors')

router.post('/', bodyParser(), async (ctx) => {
    const { body } = ctx.request.body
    const sql = `INSERT INTO authors(name) VALUES(?)`

    try {
        ctx.body = await connection.query(sql, body)
        ctx.status = 201
    } catch (err) {
        ctx.throw(err)
    }
})

router.get('/', async (ctx) => {
    const { query } = ctx
    const { limit, offset, name, sortByName } = query

    let filterValues = ""
    let filterBy = []

    filterValues += buildFilterValue('filter', 'name', name, filterValues, 'authors', ' AND', ' WHERE')
    filterBy = buildFilterOption(name, filterBy)

    let sortOptions = ""
    sortOptions += buildFilterValue('order', 'name', sortByName, sortOptions, 'authors', ',', ' ORDER BY')

    const sql = `SELECT * FROM authors${filterValues}${sortOptions}${limit ? ` LIMIT ${limit}` : ''}${offset ? ` OFFSET ${offset}` : ''}`

    try {
        ctx.body = await cacheQuery(sql, filterBy)
    } catch (err) {
        ctx.throw(err)
    }
})

router.get('/:id', async (ctx) => {
    const { query } = ctx
    const { limit, offset } = query

    const sql = `SELECT books.id, authors.name, books.title, books.description, books.image, books.date
          FROM books
            INNER JOIN book_authors
          ON book_authors.book_id = books.id
            INNER JOIN authors
          ON authors.id = book_authors.author_id
          WHERE authors.id = ?${limit ? ` LIMIT ${limit}` : ''}${offset ? ` OFFSET ${offset}` : ''}`

    try {
        ctx.body = await cacheQuery(sql, ctx.params.id)
    } catch (err) {
        ctx.throw(err)
    }

})

router.delete('/:id', async (ctx) => {
    const sql = `DELETE FROM authors WHERE id = ?`

    try {
        ctx.body = await connection.query(sql, ctx.params.id)
    } catch (err) {
        ctx.throw(err)
    }
})

router.put('/', bodyParser(), async (ctx) => {
    const { id, body } = ctx.request.body
    const sql = `UPDATE authors SET name=? WHERE id=?`

    try {
        ctx.body = await connection.query(sql, [body, id])
    } catch (err) {
        ctx.throw(err)
    }
})

module.exports = router.routes()
