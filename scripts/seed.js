const { connection } = require('../db');
const { createAuthorsTable, createBooksTable, createBookAuthorsTable, dropAuthorsTable, dropBooksTable, dropBookAuthorsTable, } = require('../lib/sql-scripts');

const ROWS = 100000;

(async () => {
    await connection.query(dropBookAuthorsTable)
    await connection.query(dropAuthorsTable)
    await connection.query(dropBooksTable)

    await connection.query(createAuthorsTable)
    await connection.query(createBooksTable)
    await connection.query(createBookAuthorsTable)

    for (let i = 1; i <= ROWS; i++) {
        await connection.query(`INSERT INTO authors(name) VALUES(?)`, ('name' + i))
    }

    for (let i = 1; i <= ROWS; i++) {
        await connection.query(`INSERT INTO books(title, description, image, date) VALUES(?, ?, ?, ?)`, ['title' + i, "desc" + i, "image" + i, Date.now()])
    }

    for (let i = 1; i <= ROWS; i++) {
        await connection.query(`INSERT INTO book_authors(author_id, book_id) VALUES(?, ?)`, [i, i])
    }
})()