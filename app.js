const Koa = require('koa')

const config = require('./lib/config')

const controllers = require('./controllers')

const app = new Koa()


app.use(controllers.routes())
app.use(controllers.allowedMethods())

module.exports = (callback) => {
  app.listen(config.port, callback)
  return app
}
