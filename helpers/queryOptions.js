const buildFilterValue = (type, colName, value, filterValues, tableName, q1, q2) => {
    if (value) {
        let result = ""
        if (filterValues[0]) {
            result += q1
        } else {
            result += q2
        }


        return (result + ` ${tableName}.${colName} ${(type === 'order' ? value : '= ?')}`)
    } else {
        return ""
    }
}

const buildFilterOption = (value,arr) => {
    if (value) {
        arr.push(value)
    }
    return (arr ? arr : [])
}

module.exports = {
    buildFilterValue,
    buildFilterOption
}